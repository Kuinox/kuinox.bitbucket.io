"use strict";
/* exported manualBallLaunching, globalValueChange*/
/* global shootBalls, randomDeg, randomVelocity*/

function manualBallLaunching(form) {
    var arr = [];
    for(var i=0; i<parseInt(form.elements[0].value); i++) {
        arr.push({
            "deg": randomDeg(),
            "velocity":randomVelocity(),
            "color": "red"
        });
    }
    shootBalls(arr, (results)=>{console.log(results);});
    return false;
}



function initPage() {
    console.log("Hello World");
}
window.onload = initPage;
