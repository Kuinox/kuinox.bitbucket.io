"use strict";   /* global Ball, shootBalls*/

//Common mathematical functions
function compareNombres(a, b) {
  return b.velocity - a.velocity;
}

Array.min = function( array ){
    return Math.min.apply( Math, array );
};

Array.max = function( array ){
    return Math.max.apply( Math, array );
};

function noise() {
    var coefNoise = 0;
    console.log(Math.random()*coefNoise-coefNoise/2);
    return Math.random()*coefNoise-coefNoise/2;
}

function randomVelocity() {
    return Math.random()*20;
}
function randomDeg() {
    return Math.random()*90;
}






function callback(velocity, deg, i) {
    finished[i] = {"velocity": velocity, "deg":deg};
    if (finished.filter(function(value) { return value.velocity !== undefined; }).length === global.sample_size) {
        relaunch();
    }
}



function relaunch() {
    finished = finished.filter(function(value){return value.velocity;}).sort(compareNombres);
    var temp_obj;
    var best_duplicate = 5;
    for(var i=0; i<global.sample_size; i++) {
        if(finished.length > 0) {
            temp_obj = finished.pop();
            new Ball(temp_obj.velocity+noise(), temp_obj.deg*90+noise(), i, "acanon", callback, "blue");
            if(best_duplicate !== 0) {
                best_duplicate --;
                i++;
                new Ball(temp_obj.velocity, temp_obj.deg*90, i  , "acanon", callback, "red");
            }
        } else {
            new Ball(randomVelocity(), randomDeg(), i, "acanon", callback, "black");
        }

    }
}
