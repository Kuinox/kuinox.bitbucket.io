"use strict";
/* exported spotCollision*/
//nombre de balle sur le terrain, limite de balle, objet de debug en affichage html

function spotCollision(aX,aY,bX,bY,cX,cY,cR) {
    return HitBoxeCollision(new Point(aX,aY), new Point(bX,bY), new Circle(cX,cY,cR));
}
function HitBoxeCollision(a,b,c) { //WIP
    var segmentAB = new Segment(a,b);
    var lineAB = segmentAB.getLine();
    var collisions = c.resolveEquationWithLine(lineAB);
    switch (collisions.length) {
        case 0:
            return b;
        case 1:
            return collisions[0];
        case 2:
            if(new Segment(a, collisions[0]).getDistancePow2() < new Segment(a, collisions[1]).getDistancePow2()) {
                return collisions[0];
            }
            return collisions[1];
    }
}


function discriminant(a,b,c) {
    let discriminant = b*b-4*a*c;
    console.log("discriminant: "+discriminant);
    if(discriminant<0) {
        return [];
    }
    if(discriminant===0) {
        return [-b/2*a];
    }
    let pre_compute = 2*a; //i have no idea how to name this, just pre calculing redondant data to avoid calculing it again.
    let part1 = -b/pre_compute;
    let sqrt_delta = Math.sqrt(discriminant)/pre_compute;
    return [part1+sqrt_delta,part1-sqrt_delta];
}

function Point(x,y){
    this.x = x;
    this.y = y;
}

Segment.prototype.getDistancePow2 = function() { //more costEffective that distance, you need to use sqrt to get a normal distance and it cost more than a simple pow"
    return (this.a.x-this.b.x)*(this.a.x-this.b.x) + (this.a.y-this.b.y)*(this.a.y-this.b.y);
};

Segment.prototype.getDistance = function() {
    return Math.sqrt(this.getDistancePow2());
};

Segment.prototype.getHeight = function () {
    if(this.gradient === undefined) {
        this.gradient = this.getGradient();
    }
    return this.a.y - this.gradient*this.a.x;
};
Segment.prototype.getGradient = function() {
    return (this.b.y-this.a.y)/(this.b.x-this.a.x);
};

Segment.prototype.getLine = function() {
    return new Line(this.getGradient(), this.getHeight());
};

function Segment(a, b) {
    this.a = a;
    this.b = b;
}

Line.prototype.getPerpendicular = function (c) {
    return new Line(1/this.gradient,c.y-this.gradient*c.x);
};

Line.prototype.getXcrossing = function(b) {
    return (b.b - this.b) / (this.a + 1 / this.a);
};

Line.prototype.pointOnLine = function(x) {
    return this.a*x+ this.b;
};

Line.prototype.getPointCrossLine = function(b) {//get point of two line crossing
    let x = this.getXCrossing(b);
    let y = this.pointOnLine(x);
    return new Point(x, y);
};

function Line(a, b) { //ax+b   a= gradient b=height
    this.a = a;
    this.b = b;
}

Circle.prototype.resolveEquationWithLine = function (line) {
    let a = line.a*line.a+1;
    let b = 2*line.a*line.b-2*line.a*this.center.y-2*this.center.x;
    let c = this.center.x*this.center.x + this.center.y*this.center.y - 2*this.center.y*line.b + line.b*line.b-20.25;
    let solutions = discriminant(a,b,c);
    let solutions_point = [];
    for (var i=0; i<solutions.length;i++) {
        solutions_point.push(new Point(solutions[i], line.a*solutions[i]+b));
    }
    return solutions_point;
};

function Circle(center, radius) {
    this.center = center;
    this.radius = radius;
}
