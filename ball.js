"use strict"; /* exported shootBalls */

var ball = {};

ball.tickrate = 100;
ball.gravity = 10;//10G
function cleanBalls() {
    let divs = document.getElementsByClassName("to-remove");
    while(divs[0]) {
        divs[0].parentNode.removeChild(divs[0]);
    }
}

function getHigherBallId() {
    var balls = Array.from(document.getElementsByClassName("ball"));
    if(balls.length ===0) {
        return 0;
    }
    return Array.max(balls.map(function(a){return parseInt(a.id);})) ;
}

Ball.prototype.tick = function(callback) { //tick to move the ball
    this.applyVelocity();
    this.applyGravity();
    let ball_div = document.getElementById(this.div_ball_id);
    let bottom = parseInt(ball_div.style.bottom, 10);
    let left = parseInt(ball_div.style.left, 10);
    var spot = {radius: 25, x: 309, y: 900};//todo : proper hitboxes
    var ball_hitbox = {radius: 5, x: bottom, y: left};
    var dx = spot.x - ball_hitbox.x;
    var dy = spot.y - ball_hitbox.y;
    var distance = Math.sqrt(dx * dx + dy * dy);

    if (distance < spot.radius + ball_hitbox.radius) {
        ball_div.className += " to-remove";
        callback(this.velocity, this.deg, this.div_ball_id);
    } else if( bottom > 0) {
        if(ball.tickrate ===0) {
            ball.tickrate = 0.000001;
        }
        window.setTimeout(() => {
            this.tick(callback);
        }, 1000/ball.tickrate);
    } else {
        ball_div.remove();
        callback(false, false, this.div_ball_id);
    }
};

Ball.prototype.applyVelocity = function() {
    let ball = document.getElementById(this.div_ball_id);
    ball.style.bottom = (parseFloat(ball.style.bottom, 10)+this.velocityX)+"px";
    ball.style.left = (parseFloat(ball.style.left, 10)+this.velocityY)+"px";
};

Ball.prototype.applyGravity = function() {
    this.velocityX -= ball.gravity/100;
};
//hitboxes management

function Ball(velocity, deg, canon, color, callback) { //degree 0 -> 90
    let ball = document.createElement("div");
        ball.id = getHigherBallId()+1;
        ball.className = "ball";
        ball.style.bottom = "340px";
        ball.style.left = "105px";
        ball.style.height = "20px";
        ball.style.width = "20px";
        ball.style.position = "fixed";
        ball.style.backgroundColor = color;
        ball.style.borderRadius = "50%";
        ball.style.opacity = "0.5";
    this.deg = deg/90;
    this.velocity = velocity;
    this.velocityX = velocity*this.deg;
    this.velocityY = velocity*(1-this.deg);
    this.div_ball_id = ball.id;

    document.getElementsByTagName("body")[0].appendChild(ball);
    document.getElementById(canon).style.transform = "rotate("+deg+"deg)";
    this.tick(callback);
}



function shootBalls(arr_obj, callback) { //shoot ball for every coordinate
    var results = [];
    for(var i=0; i<arr_obj.length; i++) { //TODO: fonction de log, tickrate global, meilleur retour des données
                                                                         /*jshint loopfunc:true */
        new Ball(arr_obj[i].velocity, arr_obj[i].deg, "acanon", arr_obj[i].color, (final_score)=> {    /*jshint loopfunc:false */
            results.push(final_score);
            if(results.length === arr_obj.length) {
                cleanBalls();
                callback(results);
            }
        });
    }
}
